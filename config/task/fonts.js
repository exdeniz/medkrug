var gulp = require('gulp'),
    // newer = require('gulp-newer'),
    font64 = require('gulp-simplefont64'),
    browserSync = require('browser-sync'),
    reload = browserSync.reload;

// gulp.task('fonts', function() {
//     gulp.src('./assets/fonts/**/*')
//         .pipe(newer('./public/fonts'))
//         .pipe(gulp.dest('./public/fonts'))
//         .pipe(reload({
//             stream: true,
//         }));

// });

gulp.task('fonts', function() {
    return gulp.src('./assets/fonts/**/*.woff')
        .pipe(font64())
        .pipe(gulp.dest('./public/css/'))
        .pipe(reload({
            stream: true,
        }));
});