var gulp = require('gulp'),
    stylus = require('gulp-stylus'),
    pixrem = require('gulp-pixrem'),
    prefix = require('gulp-autoprefixer'),
    browserSync = require('browser-sync'),
    cssBase64 = require('gulp-css-base64'),
    reload = browserSync.reload;

// Собираем Stylus
gulp.task('stylus', function() {
    gulp.src('./assets/b/blocks.styl')
        .pipe(stylus()) // собираем stylus
        .pipe(prefix({
            browsers: ['last 2 versions', 'IE 9', 'IE 10']
        }))
        .pipe(gulp.dest('./public/css/')) // записываем css
        .pipe(reload({
            stream: true
        },
            "./public/css/blocks.css"
            ));
});

gulp.task('stylusURI', function() {
    gulp.src('./assets/b/blocks.uri.styl')
        .pipe(stylus()) // собираем stylus
        .pipe(cssBase64({
            baseDir: "../../assets/icons",
        }))
        .pipe(prefix())
        .pipe(gulp.dest('./public/css/')) // записываем css
        .pipe(reload({
            stream: true
        }));
});

gulp.task('stylusIE', function() {
    gulp.src('./assets/b/blocks.ie.styl')
        .pipe(stylus()) // собираем stylus
        .pipe(prefix())
        .pipe(gulp.dest('./public/css/')) // записываем css
        .pipe(reload({
            stream: true
        }));
});

gulp.task('stylusREM', function() {
    gulp.src('./assets/b/blocks.styl')
        .pipe(stylus()) // собираем stylus
        .pipe(prefix("ie 8"))
        .pipe(pixrem('62.5%', {
            replace: true
        }))
        .pipe(rename('blockIE8.css'))
        .pipe(gulp.dest('./public/css/')) // записываем css
        .pipe(reload({
            stream: true
        }));
});