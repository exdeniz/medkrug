$(function() {
    $('a[href*=#]:not([href=#])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 700);
                return false;
            }
        }
    });
});

$('.slider').slick({
    dots: true,
    autoplay: true,
    autoplaySpeed: 10000
});

$('.protoLoginJS').click(function() {
    $('.headerNoLogin').hide();
    $('.headerLogin').show();
});

$('.protoLoginOutJS').click(function() {
    $('.headerNoLogin').show();
    $('.headerLogin').hide();
});

$('.bannerSearchButton').click(function() {
    $(this).addClass('bannerSearchButtonActive');
    $(this).parent().children('.input').addClass('inputShow');
});